package analyzer

import (
	"io"

	"gitlab.com/zrice/common-ng/vulnerability"
)

// Analyzer is an interface for all secure analyzers
type Analyzer interface {
	Analyze(r io.Reader) (*vulnerability.Report, error)
}
