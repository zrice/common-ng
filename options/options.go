package options

const (
	Run     = "run"
	Search  = "search"
	Convert = "convert"
)

type BaseOptions struct {
	TargetDir   string `env:"TARGET_DIR" default:"" long:"target-dir" help:"Target Directory"`
	ArtifactDir string `env:"ARTIFACT_DIR" default:"" long:"artifact-dir"`
}
